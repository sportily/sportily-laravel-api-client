# Sportily API Client for Laravel

Description coming soon...

## Installation

Composer is the recommended way to install this package. Add the following line to your `composer.json` file:

```json
"sportily/sportily-laravel-api-client": "dev-master@dev"
```

Then run `composer update` to get the package.

Once composer has installed the package this the following line to the `providers` array, located in your `config/app.php` file:

```php
Sportily\Api\ApiServiceProvider::class,
```

Next, run `php artisan vendor:publish` to publish this package's configuration. You should not need to edit the configuration file.
