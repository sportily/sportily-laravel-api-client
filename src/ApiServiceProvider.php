<?php
namespace Sportily\Api;

use GuzzleHttp\Client;
use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Support\ServiceProvider;
use Sportily\OAuth\Subscribers\OAuthSubscriber;

/**
 * A Laravel service provider that injects all of the Sportily API endpoints
 * into the service container.
 */
class ApiServiceProvider extends ServiceProvider {

    /**
     * Publish configuration.
     */
    public function boot() {
        $source = realpath(__DIR__ . '/../config/sportily-api.php');

        if ($this->app instanceof LaravelApplication && $this->app->runningInConsole()) {
            $this->publishes([$source => config_path('sportily-api.php')]);
        }

        $this->mergeConfigFrom($source, 'sportily-api');
    }

    /**
     * Register all of the endpoints in the service container.
     */
    public function register() {
        $config = $this->app['config']->get('sportily-api');

        // the Guzzle subscriber that injects the authorization header.
        $oauth_client = $this->app['sportily-oauth.client'];
        $oauth_subscriber = new OAuthSubscriber($oauth_client);

        // all endpoints use the same HTTP client.
        $api_client = new Client([
            'base_url' => $config['base_url'],
            'defaults' => [
                'subscribers' => [$oauth_subscriber]
            ]
        ]);

        foreach (self::$endpoints as $endpoint) {
            $this->app->singleton($endpoint, function() use($api_client, $endpoint) {
                return new $endpoint($api_client);
            });
        }
    }

    /**
     * A list of all the endpoint classes.
     */
    private static $endpoints = [
        Endpoints\AgeGroups::class,
        Endpoints\CalendarEvents::class,
        Endpoints\Clubs::class,
        Endpoints\Competitions::class,
        Endpoints\Contacts::class,
        Endpoints\DivisionEntries::class,
        Endpoints\Divisions::class,
        Endpoints\Documents::class,
        Endpoints\FixtureEvents::class,
        Endpoints\Fixtures::class,
        Endpoints\Galleries::class,
        Endpoints\Images::class,
        Endpoints\Invoices::class,
        Endpoints\Links::class,
        Endpoints\Members::class,
        Endpoints\MembersExport::class,
        Endpoints\Organisations::class,
        Endpoints\Participants::class,
        Endpoints\People::class,
        Endpoints\Posts::class,
        Endpoints\Profile::class,
        Endpoints\Registrations::class,
        Endpoints\Roles::class,
        Endpoints\Seasons::class,
        Endpoints\Teams::class,
        Endpoints\Users::class,
        Endpoints\Venues::class,
        Endpoints\Websites::class
    ];

}
