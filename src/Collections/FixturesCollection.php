<?php
namespace Sportily\Api\Collections;

use DateTime;

/**
 * A collection of fixtures.
 */
class FixturesCollection extends ApiCollection {

    /**
     * Construct a new collection, of the set of fixtures grouped by the date
     * on which they take place.
     */
    public function groupByDate() {
        return $this->groupBy(function($fixture) {
            return (new DateTime($fixture['start_time']))->format('Y-m-d');
        });
    }

}
