<?php
namespace Sportily\Api;

/**
 * An abstract Sportily API REST endpoint, exposing the standard actions of:
 * create, retreive, update, and delete.
 */
abstract class RestApiEndpoint extends ApiEndpoint {

    // The type to wrap collections in.
    protected $collection_type = Collections\ApiCollection::class;

    /**
     * Retrieve the entire collection, filtered according to the given query.
     */
    public function all($query = []) {
        $url = $this->getCollectionUrl();
        $response = $this->makeRequest('GET', $url, ['query' => $query]);
        return $this->collection_type::fromResponse($response);
    }

    /**
     * Create a new resource and add it to the collection.
     */
    public function create($body) {
        $url = $this->getCollectionUrl();
        return $this->makeRequest('POST', $url, ['body' => $body]);
    }

    /**
     * Retrieve a single resource from the collection, by unique identifier.
     */
    public function retrieve($id, $query = null) {
        $url = $this->getResourceUrl($id);
        return $this->makeRequest('GET', $url, ['query' => $query]);
    }

    /**
     * Update an existing resource in the collection.
     */
    public function update($id, $body) {
        $url = $this->getResourceUrl($id);
        return $this->makeRequest('PUT', $url, ['body' => $body]);
    }

    /**
     * Delete a resource, removing it from the collection.
     */
    public function delete($id) {
        $url = $this->getResourceUrl($id);
        return $this->makeRequest('DELETE', $url);
    }

    /**
     * Get the URL of the collection, which is the same of the root URl for
     * the endpoint.
     */
    protected function getCollectionUrl() {
        return $this->getUrl();
    }

    /**
     * Get the URL of a resource in the collection, given the unique identifier
     * of the resource.
     */
    protected function getResourceUrl($id) {
        return $this->getUrl() . '/' . $id;
    }

}
