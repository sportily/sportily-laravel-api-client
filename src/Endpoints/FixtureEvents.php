<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;

/**
 * REST endpoint for fixture events.
 */
class FixtureEvents extends RestApiEndpoint {

    protected $endpoint = 'fixture-events';

}
