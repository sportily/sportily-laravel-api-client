<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\ApiEndpoint;

/**
 * API endpoint for the logged in user's profile.
 */
class Profile extends ApiEndpoint {

    /**
     * Retrieve the profile.
     */
    public function get() {
        return $this->makeRequest('GET', $this->getUrl());
    }

}
