<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;
use Sportily\Api\Collections\FixturesCollection;

/**
 * REST endpoint for fixtures.
 */
class Fixtures extends RestApiEndpoint {

    // Use a custom collection type for fixtures.
    protected $collection_type = FixturesCollection::class;

    /**
     * Retrieve all fixtures occurring in the future.
     */
    public function future($query = []) {
        return $this->all(array_merge($query, [
            'period' => 'future'
        ]));
    }

    /**
     * Retrieve all fixtures occurring in the past.
     */
    public function past($query = []) {
        return $this->all(array_merge($query, [
            'period' => 'past'
        ]));
    }

    /**
     * Retrieve all fixtures that are currently in progress.
     */
    public function inProgress($query = []) {
        return $this->all(array_merge($query, [
            'status' => 'in_progress'
        ]));
    }

    /**
     * Retrieve all fixtures that occurred between the given dates.
     */
    public function between($start, $end, $query = []) {
        return $this->all(array_merge($query, [
            'period' => "{$start->toDateString()}:{$end->toDateString()}"
        ]));
    }

    /**
     * Retrieve fixtures for the next upcoming match day.
     */
    public function upcoming($query = []) {
        return $this->future($query)->groupByDate()->first();
    }

    /**
     * Retrieve fixtures for the most recent match day.
     */
    public function recent($query = []) {
        return $this->past($query)->groupByDate()->first();
    }

}
