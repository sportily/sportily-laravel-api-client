<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;

/**
 * REST endpoint for division entries.
 */
class DivisionEntries extends RestApiEndpoint {

    protected $endpoint = 'division-entries';

}
