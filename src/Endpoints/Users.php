<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;

/**
 * REST endpoint for users.
 */
class Users extends RestApiEndpoint {

    public function reset($id) {
        $url = $this->getResourceUrl($id) . '/reset';
        return $this->makeRequest('POST', $url);
    }

}
