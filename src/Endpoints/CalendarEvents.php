<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;

/**
 * REST endpoint for calendar events.
 */
class CalendarEvents extends RestApiEndpoint {

    protected $endpoint = 'calendar-events';

    /**
     * Retrieve a collection of all calendar events occurring between the given
     * start and end dates, optionally filtered further.
     */
    public function between($start, $end, $query = []) {
        return $this->all(array_merge($query, [
            'period' => "{$start->toDateString()}:{$end->toDateString()}"
        ]));
    }

}
