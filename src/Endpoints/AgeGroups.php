<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;

/**
 * REST endpoint for age groups.
 */
class AgeGroups extends RestApiEndpoint {

    protected $endpoint = 'age-groups';

}
