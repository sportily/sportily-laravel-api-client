<?php
namespace Sportily\Api\Endpoints;

use Sportily\Api\RestApiEndpoint;

/**
 * REST endpoint for links.
 */
class MembersExport extends RestApiEndpoint {
    protected $endpoint = 'members-export';

}
