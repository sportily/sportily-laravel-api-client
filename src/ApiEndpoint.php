<?php
namespace Sportily\Api;

use GuzzleHttp\Client;
use ReflectionClass;
use Session;

/**
 * An abstract Sportily API endpoint, which assumes a URL pattern that matches
 * the concrete class name and handles requests.
 */
abstract class ApiEndpoint {

    // The Guzzle client that handles requests.
    private $client;

    // The URL of the endpoint, defaults to the classname, lower-cased.
    protected $endpoint;

    /**
     * Construct a new API endpoint using the given HTTP client.
     */
    public function __construct($client) {
        $this->client = $client;

        if (!$this->endpoint) {
            $this->endpoint = strtolower((new ReflectionClass($this))->getShortName());
        }
    }

    /**
     * Make a request to the endpoint.
     */
    protected function makeRequest($method, $url, $payload=[]) {
        $request = $this->client->createRequest($method, $url, $payload);
        return $this->client->send($request)->json();
    }

    /**
     * Returns the root URL of the endpoint.
     */
    protected function getUrl() {
        return '/' . $this->endpoint;
    }

}
